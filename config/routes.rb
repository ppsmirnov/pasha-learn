Rails.application.routes.draw do
  get 'angular/index'

  get 'ya_copy/index'

  get 'static_pages/yandex'

  # root to: 'ya_copy#index'
  root 'angular#index'
  resources :users
  resources :articles do
    get 'filter_tags', on: :collection
  end

  resources :comments
  resources :sessions, only: [:create, :destroy]
  resources :likes
end
