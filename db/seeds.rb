# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

NUMBER_OF_USERS = 100000
NUMBER_OF_ARITCLES = 50000
NUMBER_OF_TAGS = 100
DAYS = 100
PERSENT_OF_AUTHORS = 20

# КРИТЕРИИ
# 1. 20% авторов имеют 80% статей [done]
# 2. только 40% пользователей являются авторами статей [done]
# 3. кол-во тегов = кол-во статей, но не более 100 [done]
# 4. кол-во лайков = кол-во пользовтелей * 5 [done]
# 5. кол-во тегов у статьи > 1, но  < 4 [done]
# 6. кол-во лайков у статьи =  у 10% - 100-200, у 80% - 50-70, у 10% - 0 [done]
# 7. кол-во комментов у статьи = у 10% - 50-100, у 60% - 15-30, у 30% - 0-5 [done]
# 8. кол-во пользователей = кол-во статей * 2 [done]
# 9. кол-во лайков у комментария = у 10% - 10-15, у 80% - 1-10, у 10% - 0
def like_subject(subject_id, subject_type)
  user = @all_users.sample
  Like.create!(
    user_id: user.id,
    subject_id: subject_id,
    subject_type: subject_type,
    created_at: Faker::Time.between(user[:created_at], DateTime.now),
  )
end

@comments = {}

def comment_article(article_id)
  @comments[article_id] ||= []

  has_parent = [true, false].sample
  parent_comment = @comments[article_id].sample
  if has_parent && parent_comment
    comment = Comment.new(
      body: Faker::Lorem.paragraph(1,true,1),
      user_id: @all_users.sample.id,
      article_id: article_id,
      comment_id: parent_comment
      )
  else
    comment = Comment.new(
      body: Faker::Lorem.paragraph(1,true,1),
      user_id: @all_users.sample.id,
      article_id: article_id
      )
  end
  comment[:created_at] =
      Faker::Time.between(DateTime.now - DAYS, DateTime.now)
  comment[:updated_at] = comment[:created_at]
  comment.save!

  @comments[article_id] << comment.id
end
# Deleting existing data and resetting ids
User.transaction do

  User.delete_all
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE users_id_seq RESTART WITH 1")
  Article.delete_all
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE articles_id_seq RESTART WITH 1")
  Comment.delete_all
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE comments_id_seq RESTART WITH 1")
  Tag.delete_all
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE tags_id_seq RESTART WITH 1")

  ActiveRecord::Base.connection.execute("DELETE from articles_tags")
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE articles_tags_id_seq RESTART WITH 1")
  ActiveRecord::Base.connection.execute("DELETE from likes")
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE likes_id_seq RESTART WITH 1")

  # Creating data

  # Creating users
  NUMBER_OF_USERS.times do
    picture_path = Rails.root.join("pics/#{rand(1..200)}.jpg")
    user = nil
    open(picture_path) do |picture_file|
      user = User.new(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        avatar: picture_file
      )
    end

    # Setting random create date
    user[:created_at] = user[:updated_at]  = Faker::Time.between(DateTime.now - DAYS, DateTime.now)
    user.save!
    puts "user: #{user.name} id: #{user.id}"
  end

  authors = []
  persent = (PERSENT_OF_AUTHORS + rand(-5..5)).to_f / 100
  @all_users = User.all

  while authors.size != (NUMBER_OF_USERS * persent).to_i do
    user = @all_users.sample
    unless authors.include?(user)
      authors << user
    end
  end

  # Creating articles

  NUMBER_OF_ARITCLES.times do
    size = authors.size
    r = rand(100)
    if r > 80
      a = authors[rand((size * 0.2).to_i)]
    else
      a = authors[rand((size * 0.8).to_i..(size-1))]
    end
    puts a.name
    picture_path = Rails.root.join("pics/#{rand(1..200)}.jpg")
    open(picture_path) do |picture_file|
      article = Article.new(
        title: Faker::Lorem.sentence,
        body: Faker::Lorem.paragraphs(10).join("\n"),
        picture: picture_file,
        user_id: a.id
      )
      article[:created_at] = article[:updated_at] =
        Faker::Time.between(User.find(article[:user_id])[:created_at], DateTime.now)
      article.save!
      puts article.id

    end
  end



  Article.all.each do |article|
    r = rand(100)
    if r < 11
      puts "Article: '#{article.title}' #{article.id} will have up to 200 likes"
      # like x 100-200
      rand(150..200).times do
        like_subject(article.id, 'Article')
      end
    elsif r > 10 && r < 91
      puts "Article: '#{article.title}' #{article.id} will have up to 70 likes"
      # like x 50-70
      rand(50..70).times do
        like_subject(article.id, 'Article')
      end
    else
      puts "Article: '#{article.title}' #{article.id} will have 0 likes"
    end
  end

  # Creating comments
  Article.all.each do |article|
    r = rand(100)
    if r <11
      puts "Article: '#{article.title}' #{article.id} will have up to 100 comments"
      rand(50..100).times do
        comment_article(article.id)
      end
    elsif r > 10 && r < 71
      puts "Article: '#{article.title}' #{article.id} will have up to 30 comments"
      rand(15..30).times do
        comment_article(article.id)
      end
    else
      puts "Article: '#{article.title}' #{article.id} will have up to 5 comments"
      rand(5).times do
        comment_article(article.id)
      end
    end
  end

  Comment.all.each do |comment|
    r = rand(100)
    if r < 11
      puts "Comment: '#{comment.id}' will have up to 15 likes"
      rand(10..15).times do
        like_subject(comment.id, 'Comment')
      end
    elsif r > 10 && r < 91
      puts "Comment: '#{comment.id}' will have up to 10 likes"
      rand(3..10).times do
        like_subject(comment.id, 'Comment')
      end
    else
      puts "Comment: '#{comment.id}' will have 0 likes"
    end
  end


  # Creating tags

  NUMBER_OF_TAGS.times do
    tag = Tag.new(name: Faker::Lorem.word)
    tag[:created_at] = Faker::Time.between(DateTime.now - DAYS, DateTime.now)
    tag.save
  end

  @first_tag_id = Tag.first[:id]
  @last_tag_id = Tag.last[:id]

  Article.all.each do |article|
    rand(1..3).times do
      tag_id = rand(@first_tag_id..@last_tag_id)
      article.tag_ids += [tag_id]
    end
  end


end