class AddTagRefToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :tags, index: true, foreign_key: true
  end
end
