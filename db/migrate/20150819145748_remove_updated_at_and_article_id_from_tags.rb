class RemoveUpdatedAtAndArticleIdFromTags < ActiveRecord::Migration
  def change
    remove_column :tags, :updated_at
    remove_column :tags, :article_id
  end
end
