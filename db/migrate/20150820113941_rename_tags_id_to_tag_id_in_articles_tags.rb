class RenameTagsIdToTagIdInArticlesTags < ActiveRecord::Migration
  def change
    rename_column :articles_tags, :tags_id, :tag_id
  end
end
