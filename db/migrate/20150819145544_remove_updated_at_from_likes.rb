class RemoveUpdatedAtFromLikes < ActiveRecord::Migration
  def change
    remove_column :likes, :updated_at
  end
end
