class RemoveTagsIdFromArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :tags_id
  end
end
