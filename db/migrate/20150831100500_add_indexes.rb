class AddIndexes < ActiveRecord::Migration
  def change
    add_index :articles, :user_id
    add_index :articles, :created_at
    add_index :articles_tags, :article_id
    add_index :articles_tags, :tag_id
    add_index :comments, :user_id
    add_index :comments, :article_id
    add_index :comments, :comment_id
    add_index :comments, :created_at
    add_index :likes, :subject_id
    add_index :likes, :subject_type
  end
end
