class CreateArticleTagsTable < ActiveRecord::Migration
  def change
    create_table :article_tags_tables do |t|
      t.references :article, foreign_key: true
      t.references :tags, foreign_key: true
    end
  end
end
