class RenameReferenceFieldsInLikes < ActiveRecord::Migration
  def change
    rename_column :likes, :object_id, :subject_id
    rename_column :likes, :type, :subject_type
  end
end
