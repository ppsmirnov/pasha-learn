class RenameArticlesTagsTableToArticlesTags < ActiveRecord::Migration
  def change
    rename_table :article_tags_tables, :articles_tags
  end
end
