class RemoveAllInitialIndeces < ActiveRecord::Migration
  def change
    remove_index :comments, :article_id
    remove_index :comments, :comment_id
    remove_index :comments, :user_id

    remove_index :likes, :user_id
    remove_index :articles, :user_id
  end
end
