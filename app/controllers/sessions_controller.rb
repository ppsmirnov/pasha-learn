class SessionsController < ApplicationController
	def create
		session[:user_id] = params[:id]
		redirect_to root_path
	end

	def destroy
		session.delete(:user_id)
		redirect_to root_path
	end
end
