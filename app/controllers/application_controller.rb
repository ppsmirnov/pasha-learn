class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :current_user?, :build_date_from_params

  def current_user
  	if session[:user_id]
  		@current_user ||= User.find(session[:user_id])
  	end
  end

  def build_date_from_params(field_name, params)
    return nil unless params["#{field_name}(1i)"]

      Date.new(params["#{field_name.to_s}(1i)"].to_i,
         params["#{field_name.to_s}(2i)"].to_i,
         params["#{field_name.to_s}(3i)"].to_i)
  end

  def current_user?(user)
  	user == current_user
  end
end
