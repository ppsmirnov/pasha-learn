class UsersController < ApplicationController
	before_action :correct_user,   only: [:edit, :update]
	def index
		@users = User.all.paginate(:page => params[:page], :per_page => 50)
	end

	def show
		@user = User.find(params[:id])
	end

	def edit
  	@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		if @user.update(user_params)
			redirect_to @user
		else
			render 'edit'
		end
	end
	private
	  def user_params
	    params.require(:user).permit(:name, :email, :avatar)
	  end

	  def correct_user
	  	@user = User.find(params[:id])
	  	if !current_user?(@user)
	  		flash.now[:danger] = "You have no power over here"
	  		redirect_to(user_path)
	  	else
	  		flash.now[:success] = "Success!"
	  	end
	  end
end
