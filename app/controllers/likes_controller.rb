class LikesController < ApplicationController
  def index
    query = Like.includes(:user, :subject)

    if !params[:user_id].blank?
      query = query.filter_by_user_id(params[:user_id])
    end

    @likes = query.paginate(:page => params[:page], :per_page => 5)
  end

  def new
  end

  def create
    # byebug
    # unless Like.find_by_user_id_and_subject_type(current_user.id, params[:subject_type])
      @like = Like.new
      @like.user_id = current_user.id
      @like.subject_id = params[:subject_id]
      @like.subject_type = params[:subject_type]
      @like.save!
    # end
      render js: "$('.article_like_counter').html(#{@like.subject.likes.count});"
      # respond_to do |format|
      #   format.html
      #   format.js
      # end
    # if params[:subject_type] == 'Article'
    #   redirect_to Article.find(params[:subject_id])
    # else
    #   redirect_to Article.find(Comment.find(params[:subject_id]).article_id)
    # end
  end

  def show

  end
  private
    def like_params
      params.require(:like).permit(:subject_id, :subject_type, :user_id)
    end
end
