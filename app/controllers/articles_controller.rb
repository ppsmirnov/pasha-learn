class ArticlesController < ApplicationController
	def index
    @tags = Tag.all

    article_params = params[:article]

    query = Article.includes(:user)

    unless article_params.blank?
      query = query.sort(article_params[:kind])

      if !article_params[:user_id].blank?
        query = query.filter_by_user_id(article_params[:user_id])
      end

      article_start_date = build_date_from_params("start_date", article_params)
      article_end_date = build_date_from_params("end_date", article_params)

      if !article_params[:tag_id].blank?
        query = query.filter_by_tag(article_params[:tag_id])
      end

      if !article_start_date.blank?
        query = query.filter_by_date(article_start_date, article_end_date)
      end
      @articles = query.paginate(:page => params[:page], :per_page => 5)
    else
      @articles = query.paginate(page: params[:page], :per_page => 5)
    end
	end

  def show
    @article = Article.includes(
      :user,
      comments: [:user, :likes]
    ).find(params[:id])
    @comments = build_comments_tree(@article.comments)
    # @comments = @article.first_level_comments
  end

  def new

  end
  def filter_tags
    names = Tag.parse_tags_string(params[:tags])
    existing_names = Tag.where(name: names).map(&:name)
    new_names = names - existing_names
    render json: new_names
  end
  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if @article.save
      redirect_to @article
    else
      render :new
    end
  end
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to articles_path
  end
  private
    def article_params
      params.require(:article).permit(
        :title, :body, :picture, :user_id, :tags_string
      )
    end

    def build_comments_tree(comments)
      pairs = comments.sort_by(&:created_at).map do |comment|
        [ comment.id, [comment,[],true] ]
      end
      nodes = Hash[pairs]

      nodes.each do |id, value|
        comment, _ = value
        if comment.comment_id
          nodes[comment.comment_id][1] << value
          value[2] = false
        end
      end

      nodes.values.select {|node| node[2] }
    end
end
