class CommentsController < ApplicationController
  def index

    comment_params = params[:comment]

    query = Comment.includes(:user).all
    unless comment_params.blank?
      query = query.sort(comment_params[:kind])

      if !comment_params[:user_id].blank?
        query = query.filter_by_user_id(comment_params[:user_id])
      end

      comment_start_date = build_date_from_params("start_date", comment_params)
      comment_end_date = build_date_from_params("end_date", comment_params)

      if !comment_start_date.blank?
        query = query.filter_by_date(comment_start_date, comment_end_date)
      end
      @comments = query.paginate(:page => params[:page] , :per_page => 5)
    else
      @comments = query.paginate(page: params[:page], :per_page => 5)
    end

  end

  def new
    # @comment.article_id = params[:article_id]
  end

  def create
    # byebug
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    @comment.article_id = params[:article_id]
    if !params[:comment_id].blank?
      @comment.comment_id = params[:comment_id]
    end
    @comment.save
    redirect_to article_path(Article.find(params[:article_id]))
  end

  private
    def comment_params
      params.require(:comment).permit(:body, :user_id, :article_id, :comment_id)
    end
end
