class Like < ActiveRecord::Base
	belongs_to :user
	belongs_to :subject, polymorphic: true


  def self.filter_by_user_id(user_id)
    self.where(user_id: user_id)
  end
end
