class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :likes,  as: :subject
  has_and_belongs_to_many :tags
  has_attached_file :picture
  validates_attachment_content_type :picture, :content_type => /^image\/(bmp|gif|jpg|jpeg|png)/
  validates :title, presence: true
  validates :body, presence: true
  validates :picture_file_name, presence:true

  def self.filter_by_user_id(user_id)
    self.where(user_id: user_id)
  end

  def self.filter_by_tag(tag_id)
    joins(:tags).where(tags: {id: tag_id})
  end


  def self.filter_by_date(start_date, end_date)
    where('articles.created_at >= ? and articles.created_at <= ?',
      start_date, end_date)
  end

  def self.sort(kind='desc')
    if kind == 'desc'
      self.order("articles.created_at desc")
    elsif kind == 'likes_desc'
      self.joins("LEFT OUTER JOIN likes ON likes.subject_id = articles.id").group("articles.id").order("count(likes.subject_id) desc")
    elsif kind == 'likes_asc'
      self.joins("LEFT OUTER JOIN likes ON likes.subject_id = articles.id").group("articles.id").order("count(likes.subject_id) asc")
    else
      self.order("articles.created_at asc")
    end
  end

  def first_level_comments
    comments.where('comment_id is null')
  end

  def short_body
    body.split[0..10].join(' ')
  end

  def tags_string=(string)
    names = Tag.parse_tags_string(string)
    self.tags = names.map do |name|
      Tag.find_or_create_by!(name: name)
    end
  end

  def tags_string
    tags.map(&:name).join(', ')
  end
end
