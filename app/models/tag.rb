class Tag < ActiveRecord::Base
  has_and_belongs_to_many :articles

  def self.parse_tags_string(string)
    return [] if string.blank?
    string.split(',').map(&:strip).reject(&:blank?)
  end
end
