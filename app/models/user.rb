class User < ActiveRecord::Base
	has_many :articles
	has_many :comments
	has_many :likes
	has_many :liked_comments, through: :likes, source: :subject, source_type: 'Comment'
	has_many :liked_articles, through: :likes, source: :subject, source_type: 'Article'
	has_attached_file :avatar
	validates_attachment_content_type :avatar, :content_type => /^image\/(bmp|gif|jpg|jpeg|png)/
	validates :name, presence: true
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }
end
