class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :article
  belongs_to :comment
  has_many :likes, as: :subject
  has_many :comments
  validates :body, presence: true

  def self.filter_by_user_id(user_id)
    self.where(user_id: user_id)
  end

  def self.filter_by_date(start_date, end_date)
    where("comments.created_at >= ? and comments.created_at <=?",
      start_date, end_date)
  end

  def self.sort(kind='desc')
    if kind == 'desc'
      self.order("comments.created_at desc")
    elsif kind == 'likes_desc'
      # self.includes(:likes).order("likes.subject_id desc")
      self.joins("LEFT OUTER JOIN likes ON likes.subject_id = comments.id").group("comments.id").order("count(likes.subject_id) desc")
    elsif kind == 'likes_asc'
      self.joins("LEFT OUTER JOIN likes ON likes.subject_id = comments.id").group("comments.id").order("count(likes.subject_id) desc")
    else
      self.order("comments.created_at asc")
    end
  end
end
