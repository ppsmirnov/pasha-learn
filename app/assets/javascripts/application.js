// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require angular
//= require_tree .

var TagFilter = function($input) {

  TagFilter.alarm = function(){
    $('.new-tags').click(function(){
      console.log(TagFilter.savedTags);
    });
  };

  if ($input.length == 0) return;

  var tags = function() {
    var val = this.value;
    var newTags = $('.new-tags');
    var str = ['/articles/filter_tags?tags=', val].join('')

    $.get(str, function(response) {
      newTags.html(response.join(', '));
      TagFilter.savedTags  = response.join(', ')
    });


    TagFilter.alarm();
  };

  $input.on('input', tags);
};



$(function() {
  var tagsInput = $('.tags-input');
  // alarm();
  obj = new TagFilter(tagsInput)

});
