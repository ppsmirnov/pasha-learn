# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

app = angular.module 'app', []

app.controller 'testCtrl', ($scope) ->
  # $scope.progress = 22
  $scope.focused = false
  $scope.obj = forcedisplay: true
  setInterval ->
    # $scope.progress += 5
    # $scope.focused = !$scope.focused
    $scope.obj.forcedisplay = !$scope.obj.forcedisplay
    $scope.$apply()
  , 200


app.directive 'elasticInput', () ->
  restrict: 'A'
  link: (scope, element, attrs) ->
    helper = $('<pre />')
      .css
        position: 'absolute'
        top: '-1000px'
        font: element.css('font')
      .appendTo document.body

    setAppropriateInputWidth = ->
      text = element.val() || element.attr('placeholder')
      helper.text(text)
      element.width helper.width()

    scope.$watch (-> element.val()), setAppropriateInputWidth
    setAppropriateInputWidth()

app.directive 'tooltip', () ->
  restrict: 'A'
  scope:
    tooltipDisplayOnMouseenter: "="
    tooltipForceDisplay: "="

  link: (scope, element, attrs) ->
    div = document.createElement('div')
    div.style.display = "none"
    div.setAttribute("class", "tooltip #{ attrs.tooltipClass}")
    div.innerHTML = attrs.tooltip
    element.context.appendChild(div)

    forceDisplay = ->
      if scope.tooltipForceDisplay
        element.find('.tooltip').show()
      else
        element.find('.tooltip').hide()

    element.on 'mouseenter', ()->
      element.find('.tooltip').show() if scope.tooltipDisplayOnMouseenter

    element.on 'mouseleave', ()->
      element.find('.tooltip').hide() if scope.tooltipDisplayOnMouseenter

    scope.$watch 'tooltipForceDisplay', forceDisplay

app.directive 'progressbar', () ->
  restrict: 'E'
  templateUrl: 'js-views/progressbar.html'
  scope: progress: '='

  link: (scope, element, attrs) ->
    updateProgress = () ->
      if scope.progress?
        bar = element.find('.progress')
        width = Math.min(100, scope.progress)
        bar.css width: "#{width}%"
      else
        element.hide()

    scope.$watch('progress', updateProgress)

app.directive 'focused', () ->
  restrict: 'A'
  scope: focused: '='

  link: (scope, element, attrs) ->
    updateFocus = ->
      if scope.focused
        element.context.focus()
      else
        element.context.blur()

    element.on 'blur', () ->
      scope.focused = false
      updateFocus()

    element.on 'focus', () ->
      scope.focused = true
      updateFocus()

    scope.$watch('focused', updateFocus)


app.directive 'tweetInput', () ->
  restrict: 'E'
  templateUrl: 'js-views/tweet-input.html'
  scope:
    maxlength: '='
    value: '='

  link: (scope, element, attrs) ->
    scope.forceDisplay = false
    scope.focused = true

    input = element.find('span input')
    input.attr('maxlength', scope.maxlength);

    scope.$watch 'value', ->
      if scope.value.length >= scope.maxlength
        scope.forceDisplay = true
      else
        scope.forceDisplay = false
      scope.progress =  scope.value.length / scope.maxlength * 100

    $(document.body).on 'keydown', (event) ->
      if event.keyCode == 27
        scope.focused = !scope.focused
