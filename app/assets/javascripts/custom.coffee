isObject = (value) ->
  value.constructor == Object unless typeof(value) == "undefined"

isArray = (value) ->
  value.constructor == Array unless typeof(value) == "undefined"

mapKeys = (object, fn) ->
  if isArray(object)
    mapKeys(value, fn) for value in object
  else if isObject(object)
    result = {}
    for key, value of object
      result[fn(key)] = mapKeys(value, fn)
    result
  else
    object

capitalize = (string) ->
  string[0].toUpperCase() + string.slice(1)

camelCase = (value) ->
  parts = for value, index in value.split('_')
    if index == 0
      value
    else
      capitalize(value)
  parts.join('')

snakeCase = (value) ->
  result = ''
  for char, index in value
    if char == char.toUpperCase()
      result = result + '_' + char.toLowerCase()
    else
      result = result + char
  result

hasSemicolon = (string) ->
  if string.indexOf(':') > -1
    return true
  else
    return false

objectKeys = (object) ->
  key for key, _ of object

isEqual = (a, b) ->
  return a == b if !a || !b

  if a.constructor != b.constructor
   return false

  if isArray(a)
    if a.length != b.length
      return false
    for value, index in a
      if !isEqual(value, b[index])
        return false
    true
  else if isObject(a)
    if objectKeys(a).length != objectKeys(b).length
      return false
    for key, value of a
      if !isEqual(value, b[key])
        return false
    true
  else
    a == b

assertEqual = (expected, got) ->
  expectedJson = JSON.stringify(expected)
  gotJson = JSON.stringify(got)
  if isEqual(expected, got)
    console.info("OK!\nExpected: #{expectedJson}\n     got: #{gotJson}")
  else
    console.error("Assertion error!\nExpected: #{expectedJson}\n     got: #{gotJson}")

getAllSubstrings = (string) ->
  result = []
  substring = ''
  escape = false
  for char, index in string
    # if substring has leading space, delete that space
    substring = substring.slice(1) if substring[0] == ' '
    # if char is " or [, start reading substring
    if (char == '"' or char == '[') or isSubstring
      isSubstring = true
      substring += char
      if (char == '"'  or char == ']') and substring.length > 1 and !escape
      # end of substring, unless previous char was \
        result.push(substring)
        substring = ''
        isSubstring = false
    # handling back slash
      if char == "\\"
        escape = true
      else
        escape = false
    # handling substring, that doesn't start with " or [
    else
      substring += char unless (char == ',' or char == ':')
      if (char == ',' or char == ':')  or index == string.length - 1
         # end of substring
        result.push(substring) unless substring == ''
        substring = ''

  result

parseString = (string) ->
  array = string.split(', ')
  object = {}

  positionedParamsString = ''
  for value, index in array
    if !hasSemicolon(value)
      positionedParamsString += value + ', '
    else
      break

  positionedParamsString = positionedParamsString.slice(0, -2)
  positionedParameters = getAllSubstrings(positionedParamsString)

  namedParamsString = string.slice(positionedParamsString.length)
  namedParams = getAllSubstrings(namedParamsString)
  i = 0
  while i < namedParams.length
    object[namedParams[i]] = namedParams[i+1]
    i += 2

  return [positionedParameters, object]

assertParse = (object, string) ->
  assertEqual object, parseString(string)

assertPositionalParse = (array, string) ->
  assertEqual array, parseString(string)[0]

$ ->
  testValue = [
    {foo_bar: 12}
    {gazVaz: {baz_maz: [1, 2, 3]}}
    3
    {taz_paz: 4}
  ]

  # console.log camelCase("foo_bar_baz")
  # console.log snakeCase("fooBarBaz")
  # console.log mapKeys(testValue, camelCase)

  # assertEqual [1, foo: 1], [1, foo: 1]

  # assertParse [['foo', 'bar'], {}], "foo, bar"
  # assertPositionalParse ['foo'], 'foo'
  # assertParse [ ['foo', 'bar'], {baz: 'gaz'} ], "foo, bar, baz: gaz"
  # assertPositionalParse ['foo'], 'foo'
  # assertPositionalParse ['foo', 'bar'], 'foo, bar'
  # assertParse [ [], {foo: '"bar: baz, gaz: vaz"', maz: '[1, 2, 3, {paz: taz}]'} ],
  # 'foo: "bar: baz, gaz: vaz", maz: [1, 2, 3, {paz: taz}]'
  # assertPositionalParse ['foo', 'bar', '"gaz, baz"'], 'foo, bar, "gaz, baz"'
  # assertParse [ ['"some string"'], {another_string: '123', "1 + 1": '"3"'} ],
  # '"some string", another_string: 123, 1 + 1: "3"'
  #
  # assertPositionalParse ['"foo\\"bar"'], '"foo\\"bar"'
